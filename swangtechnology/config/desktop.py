# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from frappe import _

def get_data():
	return [
		{
			"module_name": "Swangtechnology",
			"color": "yellow",
			"icon": "octicon database",
			"type": "module",
			"label": _("Swangtechnology")
		}
	]
